def tervitus(tervituseNr):
    print('Võõrustaja: "Tere!"')
    print('Täna ' + str(tervituseNr) + '. kord tervitada, mõtiskleb võõrustaja.')
    print('Külaline: "Tere, suur tänu kutse eest!"')
    
külalisteArv = int(input("Sisesta külaliste arv: "))
n = 1
while külalisteArv > 0:
    tervitus(n)
    n += 1
    külalisteArv -= 1