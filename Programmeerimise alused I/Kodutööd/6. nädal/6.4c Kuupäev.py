def kuu_nimi(järjekorraNr):
    kuud = ['jaanuar', 'veebruar', 'märtrs', 'aprill', 'mai', 'juuni', 'juuli', 'august', 'september', 'oktoober', 'november', 'detsember']
    kuu = kuud[järjekorraNr - 1]
    return kuu.lower()

def kuupäev_sõnena(päev):
    hakitud = päev.split(".")
    kuupäev = hakitud[0] + ". " + kuu_nimi(int(hakitud[1])) + " " + hakitud[2] +". a"
    return kuupäev

küsimus = input("Sisesta kuupäev kujul DD.MM.YYYY: ")

print(kuupäev_sõnena(küsimus))