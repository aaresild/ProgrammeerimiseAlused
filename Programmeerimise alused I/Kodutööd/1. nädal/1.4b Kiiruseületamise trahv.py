nimi = input('Sisestage oma nimi: ')
lubatudKiirus = int(input("Sisestage lubatud kiirus (km/s): "))
tegelikKiirus = int(input("Sisestage tegelik kiirus (km/s): "))

arvutatudTrahv = (tegelikKiirus - lubatudKiirus) * 3
trahv = min(190, arvutatudTrahv)

print(nimi + ", kiiruse ületamise eest on trahv teile " + str(trahv) + "€.") 


