from tkinter import *

raam = Tk()
raam.title("Maja")
tahvel = Canvas(raam, width=800, height=800, background="white")

tahvel.create_rectangle(50, 350, 750, 750, fill="darkred", outline="darkred")
tahvel.create_polygon(50, 350, 400, 50, 750, 350, fill="#976633", outline="#976633")
tahvel.create_rectangle(600, 600, 700, 750 , fill="saddlebrown", outline="saddlebrown")

aknavahevahe = 50
i = 1

while i < 12:
    if i != 4:
        tahvel.create_rectangle(aknavahevahe * i + aknavahevahe, 400, aknavahevahe * i + aknavahevahe + aknavahevahe, 500, fill="skyblue", outline="saddlebrown")
        tahvel.create_rectangle(aknavahevahe * i + aknavahevahe * 2, 400, aknavahevahe * i + aknavahevahe * 3, 500, fill="skyblue", outline="saddlebrown")
    if i != 4 and i < 7:
        tahvel.create_rectangle(aknavahevahe * i + aknavahevahe, 600, aknavahevahe * i + aknavahevahe + aknavahevahe, 700, fill="skyblue", outline="saddlebrown")
        tahvel.create_rectangle(aknavahevahe * i + aknavahevahe * 2, 600, aknavahevahe * i + aknavahevahe * 3, 700, fill="skyblue", outline="saddlebrown")
      
    i += 5


tahvel.pack()
raam.mainloop()