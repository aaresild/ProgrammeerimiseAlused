from tkinter import *

raam = Tk()
raam.title("Kareda valla lipp")
tahvel = Canvas(raam, width=1100, height=700, background="white")
kõrgus = 350
i = 0

while i < 2:
    tahvel.create_polygon(0, i * kõrgus, 1100, kõrgus / 2 + i * kõrgus, 0, kõrgus + i * kõrgus, fill="deepskyblue", outline="deepskyblue")
    i += 1
    
tahvel.pack()
raam.mainloop()