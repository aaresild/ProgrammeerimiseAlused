leeduPerenimi = input("Sisestage Leedu perekonnanimi: ")

if leeduPerenimi[-2:] == "ne":
    print("Abielus")
elif leeduPerenimi[-2:] == "te":
    print("Vallaline")
elif leeduPerenimi[-1:] == "e" and (leeduPerenimi[-2:] != "te" or
                                    leeduPerenimi[-2:] != "ne"):
    print("Määramata")
else:
    print("Pole ilmselt leedulanna perekonnanimi")
    