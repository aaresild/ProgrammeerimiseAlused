from random import randint

aknatõenäosus = randint(1,3)

iseVõiLoos = input("Kas soovite istekoha valida ise või loosida? Sisesta 'ise' või 'loos': ")
koht = ""

if iseVõiLoos == "ise":
    akenVõiMuu = input("Kas soovid istuda akna ääres või mujal? Sisesta 'aken' või 'muu': ")
    if akenVõiMuu == "aken":
        koht = "Aknakoht"
    elif akenVõiMuu == "muu":
        koht = "Vahekäigukoht"
    print("Valisite ise.", koht)

elif iseVõiLoos == "loos":
    if aknatõenäosus == 1:
        koht = "Aknakoht"
    else:
        koht = "Vahekäigukoht"
    print("Istekoht loositi.", koht)


    
    