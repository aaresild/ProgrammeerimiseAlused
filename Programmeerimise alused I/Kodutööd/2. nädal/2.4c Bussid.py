from math import ceil

inimesteArv = int(input("Sisesta inimeste arv: "))
kohtiBussis = int(input("Sisesta kohtade arv bussis: "))
busseVaja = ceil(inimesteArv / kohtiBussis)
viimasesBussisInimesi = inimesteArv % kohtiBussis

if viimasesBussisInimesi == 0:
    viimasesBussisInimesi = kohtiBussis

print("Busse vaja: ", busseVaja)
print("Viimases bussis inimesi: ", viimasesBussisInimesi)