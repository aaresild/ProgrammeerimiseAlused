vanus = int(input("Sisestage enda vanus: "))
sugu = input("Sisestage enda sugu: ").lower()
trenniTüüp = int(input("Sisestage trenni tüüp (1 - tervis, 2 - vastupidavus, 3 - intensiivne): "))

meesteSoovituslik = 220 - vanus
naisteSoovituslik =  206 - vanus * 0.88

if sugu == "m":
    soovituslik = meesteSoovituslik
elif sugu == "n":
    soovituslik = naisteSoovituslik

if trenniTüüp == 1:
    vähimPulss = soovituslik * 0.5
    suurimPulss = soovituslik * 0.7
elif trenniTüüp == 2:
    vähimPulss = soovituslik * 0.7
    suurimPulss = soovituslik * 0.8
elif trenniTüüp == 3:
    vähimPulss = soovituslik * 0.8
    suurimPulss = soovituslik * 0.87

print("Pulsisagedus peaks olema vahemikus", str(round(vähimPulss)), "kuni", str(round(suurimPulss)) + ".")