failinimi = input("Palun sisestage failinimi: ")

fail = open(failinimi, encoding="UTF-8")
sihtkohad = []

for i in fail:
    sihtkohad += [i.strip(" \n")]
    
print("Võimalikud sihtkohad:")
järts = 1
for j in sihtkohad:
    print(str(järts) + ". " + str(j))
    järts += 1
    
valik = int(input("Valige, mitmes sihtkoht broneerida: "))
print("Reis on broneeritud. Sihtkoht on " + sihtkohad[valik - 1] + ".")

fail.close()