import re

nummer = input("Sisesta number: ")

if re.match('^[0-9]{3}[A-Z]{3}$', nummer):
    print('On Eesti registreerimisnumber')
else:
    print('Ei ole Eesti registreerimisnumber')
