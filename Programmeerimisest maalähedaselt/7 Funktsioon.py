'''
def trükiAB():
    print("A")
    print("B")
    
print("C")
trükiAB()
print("D")
trükiAB()
'''
'''
trükiAB()
def trükiAB():
    print("A")
    print("B")
'''

'''
from time import sleep
 
def loe_alla(i):
    while i > 0:
        
        i -= 1
        print(i)
        sleep(1)
 
loe_alla(9)
'''
'''
def ruutu(x):
    return x**2
print(ruutu(4))
'''

'''
def kas_raha_jätkub(kghind, kogus, raha):
    hind = kghind * kogus
    if hind <= raha:
        return True
    else:
        return False

if kas_raha_jätkub(8, 4.5, 87):
    print("Ostan")
else:
    print("Raha jääb väheks")
    '''
'''
def summa(x,y):
    return x + y
print(summa(1,3))
'''
'''
def summa(x,y):
    summa = x + y
    return summa
print(summa(1,3))
'''
'''
from turtle import *
 
def ruut(külg):            # Defineerime funktsiooni nimega ruut, mille argumendiks on ruudu külje pikkus
    i = 0
    while (i < 4):
        forward(külg)      # Siin kasutataksegi argumenti
        left(90)
        i = i + 1
 
ruut(50)                   # Kilpkonn joonistab ruudu küljega 50 pikslit
ruut(75)                   # Kilpkonn joonistab ruudu küljega 75 pikslit
 
exitonclick()
'''
'''
from turtle import *
 
def ruut(n):               # Defineerime funktsiooni ruudu joonistamiseks
    i = 0
    while (i < 4):
        fd(n)
        lt(90)
        i = i + 1
 
pencolor("#32CD32")        # Kilpkonn muudab pliiatsi värvi laimiroheliseks
fillcolor("red")           # Kilpkonn muudab täitevärvi punaseks
begin_fill()               # Kilpkonn alustab ringi värvimist
circle(100)                # Kilpkonn joonistab ringi raadiusega 100 pikslit
end_fill()                 # Kilpkonn lõpetab ringi värvimise
 
up()                       # Pliiats üles
fd(300)                    # Kilpkonn liigub edasi 100 pikslit
lt(90)                     # Kilpkonn pöörab 90° vasakule
fd(50)
down()                     # Pliiats alla
 
pencolor("red")            # Kilpkonn muudab pliiatsi värvi
fillcolor("#32CD32")       # Kilpkonn muudab pliiatsi värvi punaseks, täitevärvi laimiroheliseks
begin_fill()
ruut(100)                  # Kilpkonn joonistab ruudu küljega 100 pikslit
end_fill()
 
bgcolor("pale turquoise")  # Muudame taustavärvi helesiniseks
 
exitonclick()
'''
'''
from turtle import *

def hulknurk(n, külg):
    i = 0
    nurk = 360 / n
    while (i < n):
        fd(külg)
        lt(nurk)
        i += 1
fillcolor('green')
begin_fill()
hulknurk(15, 50)
end_fill()

fillcolor('red')
begin_fill()
hulknurk(3, 50)
end_fill()
exitonclick()
'''
'''
from turtle import *
 
def silm():                # Defineerime funktsiooni silmade joonistamiseks     
    pencolor("#000000")
    fillcolor("#FFFFFF")
    begin_fill()
    circle(25)
    end_fill()
 
pencolor("#000000")        # Pea
fillcolor("#FFFF00")  
begin_fill()
circle(100)                 
end_fill()
 
up()                              
bk(45)
lt(90)
fd(100)
rt(90)
down()
 
silm()                     # Vasak silm
 
up()                             
fd(90)
down()
 
silm()                     # Parem silm
 
up()                       # Suu
bk(95)
rt(90)
fd(30)
down()
circle(50,180)             # Pool ringjoonest
bgcolor("#AFEEEE")
 
exitonclick()
'''

'''
from turtle import *
 
# Defineerime funktsiooni täht, mis joonistab valitud värvi ja pikkusega tähe
def täht(pikkus, värv):   
    color(värv)
    begin_fill()                 
    i = 0
    while (i < 5):
        fd(pikkus)
        rt(144)
        i = i + 1
    end_fill()
 
täht(200, "yellow")        # Kilpkonn joonistab kollase tähe pikkusega 100 pikslit
 
exitonclick()
'''

from turtle import *

def varjutus():
    pencolor('yellow')
    fillcolor('black')
    begin_fill()
    circle(100)
    end_fill()

varjutus()
bgcolor('black')

exitonclick()
