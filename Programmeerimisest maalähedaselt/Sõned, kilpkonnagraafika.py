'''
print("Rock 'n' roll")
print('Jim ütles vaid: "Siin see on."')

print("Jack vastas: \"Rock 'n' roll\".")
print('Jack vastas: "Rock \'n\' roll!.')
'''
'''
print("Sõida\nseenele\nahv")

print("C:\\kasutaja\\fail.txt")
'''
'''
print('Minu leemikraamatud on \n\"Kevade\" ja \n\"Rehepapp\"')
'''


#print("""Jack vastas: "Rock 'n' roll".""")
#print('''Jack vastas: "Rock 'n' roll".''')
#print("""Rock 'n' rolli Jim ütles vaid: "Siin see on." """)

'''
ees = "Kukk"
pere = "Kikk"
vanus = 22
print(ees + " " + pere + " vanus: " + str(vanus))
print(ees, pere, "vanus:", vanus)
'''
'''
nimi1 = "Otto"
nimi2 = "Triin"
print("Tere,", nimi1)
print(nimi2 + "!")
'''
'''
print("tartu".capitalize())
print("Tartu".endswith("tu"))
print("Tartu".lower())
print("Tartu".upper())
print("Tartu".startswith("tu"))
print("Kauneim linn on Eestis Tartu".title())
'''
'''
from turtle import *
forward(100)
right(144)
fd(100)
rt(144)
fd(100)
rt(144)
fd(100)
rt(144)
fd(100)

color("cyan")
begin_fill()
circle(50)
end_fill()

exitonclick()
'''
'''
from turtle import *
from random import randint

värv = randint(1, 3)
if värv == 1:
    color("blue")
elif värv == 2:
    color("black")
elif värv == 3:
    color("white")
begin_fill()
circle(100)
end_fill()

exitonclick()
'''

ku = "mm"
os = 22

if ku == "mm":
    if os > 11:
        print("enam")
    elif os < 11:
        print("vähem")
    else:
        print("Võrdne")
else:
    print("Vale")

