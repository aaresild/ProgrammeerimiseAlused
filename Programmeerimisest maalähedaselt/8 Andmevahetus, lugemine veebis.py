'''
from urllib.request import urlopen
 
vastus = urlopen("http://kodu.ut.ee/~eno/mooc/maa.txt")
 
baidid = vastus.read()
# veebist lugemisel annab käsk read() meile tavalise sõne asemel hunniku baite,
# mis on vaja veel sõneks "dekodeerida"
tekst = baidid.decode()
vastus.close()
 
print(tekst[10].upper() + tekst[-2] + tekst[-2])
'''

a = [1, 4, 5, -27]
i = 0
while i < len(a):
    print(a[i])
    i += 1
