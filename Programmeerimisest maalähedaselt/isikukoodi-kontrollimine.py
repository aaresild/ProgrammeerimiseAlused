import re
isikukood = input("Palun sisesta isikukood: ")

# teeme isikukoodi 10-nest esimesest numbrist loendi Eesti isikukoodi kontrollnumbri arvutamiseks

isikukood_10_numbrit = [int(x) for x in str(isikukood[:10])] 

# Eesti isikukoodi kontrollnumbri arvutamise numbriseeriad

kontroll_1 = [1,2,3,4,5,6,7,8,9,1] 
kontroll_2 = [3,4,5,6,7,8,9,1,2,3]

# Eesti isikukoodi kontrollnumbri arvutamine mõlema numbriseeriaga

kontrollarvutus_1 = sum(x * y for x, y in zip(isikukood_10_numbrit, kontroll_1)) % 11
kontrollarvutus_2 = sum(x * y for x, y in zip(isikukood_10_numbrit, kontroll_2)) % 11

# Kontrollnumbri leidmine vastavalt kehtivatele reeglitele

if kontrollarvutus_1 == 10 and kontrollarvutus_2 == 10:
    kontroll = 0
elif kontrollarvutus_1 == 10:
    kontroll = kontrollarvutus_2
else:
    kontroll = kontrollarvutus_1

# Võrdlus:
# Kas isikukood on 11 märki, 
# Kas kontrollnumber on õige 
# Kas isikukoodi numbrid on reeglitega vastavuses

if len(isikukood) == 11 and int(isikukood[10]) == kontroll and re.match("^[1-6][0-9]{2}[0-1]{1}[0-9]{1}[0-3]{1}[0-9]{5}$", isikukood):
    print("See on Eesti isikukood")
else:
    print("See ei ole Eesti isikukood")