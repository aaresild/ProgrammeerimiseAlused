'''
i = 0
while i < 5:
    print("Tere!")
    i = i + 1
    print(i)
'''
'''
from turtle import *

i = 0
while i < 3:
    forward(100)
    left(120)
    i = i + 1

exitonclick()
'''
'''
print("Sisesta PIN-kood:")
sisestatud_pin = input()
while sisestatud_pin != "1234":
    print("Sisesta PIN-kood:")
    sisestatud_pin = input()
print("Sisenesid pangaautomaati!")
'''

sisestatud_pin = ""
katseid = 3
'''
while sisestatud_pin != "1234" and katseid > 0:
    print("Sisesta PIN-kood:")
    print("Jäänud on ", str(katseid), "katset.")
    katseid -= 1
    sisestatud_pin = input()

if sisestatud_pin == "1234":
    print("Sisenesid pangaautomaati!")
else:
    print("Kaart on lukus.")
'''
'''
from time import sleep
sis_pin = ''
katseid = 3
while sis_pin != '1234' and katseid > 0:
    print("Sisesta PIN:")
    print("Katseid on jäänud:", str(katseid))
    sis_pin= input()
    katseid -= 1
if sis_pin == '1234':
    print("Sisenesid automaati!")
else:
    print("Enesehävitus aktiveeritud.")
    i = 5
    while i > 0:
        print(i)
        i -= 1
        sleep(1)
    print('"Pau"')
'''
'''
from random import randint

arv = randint(1, 5)
arvamus = ''
katseid = 3

while arvamus != arv and katseid > 0:
    print('Sisesta nr 1-5')
    print('Sul on selleks veel', katseid, 'katset.')
    arvamus = int(input())
    katseid -= 1
    if arv > arvamus:
        print("Minu arv on suurem.")
        print('Arva veel!')
    elif arv < arvamus:
        print("Minu arv on väiksem.")
        print('Arva veel!')
        

if arv == arvamus and katseid == 2:
    print("Oled vist selgeltnägija.")
if arv == arvamus:
    print("Õige vastus!")
else:
    print("Kaotasid mängu.")
'''


from random import randint

arv = randint(1, 5)
arvamus = ''
katseid = 3

while arvamus != arv and katseid > 0:
    print('Sisesta nr 1-5')
    print('Sul on selleks veel', katseid, 'katset.')
    arvamus = randint(1, 5)
    katseid -= 1
    if arv > arvamus:
        print("Minu arv on suurem.")
        print('Arva veel!')
    elif arv < arvamus:
        print("Minu arv on väiksem.")
        print('Arva veel!')
        

if arv == arvamus and katseid == 2:
    print("Oled vist selgeltnägija.")
if arv == arvamus:
    print("Õige vastus!")
else:
    print("Kaotasid mängu.")




