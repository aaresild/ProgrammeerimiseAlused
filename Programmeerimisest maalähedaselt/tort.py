from math import ceil
laius = int(input("Mitu küpsist on tordi laius?"))
pikkus = int(input("Aga pikkus?"))
korrus = int(input("Mitme korruselist torti soovid?"))
arv_pakis = int(input("Mitu küpsist on ühes pakis?"))

küpsiseid_vaja = laius * pikkus * korrus
pakke_vaja = ceil(küpsiseid_vaja / arv_pakis)


print("Vaja läheb " + str(küpsiseid_vaja) + " küpsist, seega tuleks osta " + str(pakke_vaja) + " pakk(i) küpsiseid.")
    