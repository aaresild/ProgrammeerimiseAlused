isaPalk = int(input('Sisestage isa palk: '))
emaPalk = int(input('Sisestage ema palk: '))
lasteArv = int(input('Sisestage laste arv: '))

def netopalk(a):
    if a > 170:
        palk = ((a - 170) * 0.8) + 170
    else:
        palk = a
    return round(palk, 2)

sissetulek = netopalk(isaPalk) + netopalk(emaPalk) + (lasteArv * 50)
print('Pere kuusissetulek on', sissetulek, 'eurot')