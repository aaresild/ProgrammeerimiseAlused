def liida_tekst(järjend):
    sõne = ''
    for element in järjend:
        if not isinstance(element, list):
            sõne += element            
        else:            
            sõne += liida_tekst(element)    
    return sõne
    
a = ['a', ['c']] # 'ac'
b = ['a', 'b', ['c', ['u', []], ['x'], 'r', [[['r']]]]] # 'abcuxrr'
c = ['a', 'b', ['c, [d,e], e']] # 'abc, [d,e], e'
d = [[], []] # ''

print(liida_tekst(d))
