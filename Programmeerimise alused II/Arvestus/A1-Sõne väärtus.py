def sõne_väärtus(sõne):    
    täheväärtusteSumma = 0    
    for täht in sõne:
        täheväärtusteSumma += ord(täht)
    return täheväärtusteSumma
        
def säsvi(tabel):    
    veerud = []    
    for järjend in tabel:             
        for i in range(len(järjend)):
            veerg = [järjend[i]]
            
            if len(veerud) == i:
                veerud += [veerg]
            else:
                veerud[i] += veerg
    #print(veerud)
    for i in range(len(veerud)):
       veerud[i] = sõne_väärtus(''.join(veerud[i]))
        
    suurimVeeruindeks = 0
    veeruVäärtus = 0
    for i in range(len(veerud)):
        if veeruVäärtus <= veerud[i]:
            veeruVäärtus = veerud[i]
            suurimVeeruindeks = i
    return suurimVeeruindeks    

sõned = [["A", "A1aa", "Tere, kevad!", " "],
         ["Tere, kevad!", "A", "A1aa", "A1aa", 'muusa'],
         ["A1aa", "A", "A", "a"]]

print(säsvi(sõned))