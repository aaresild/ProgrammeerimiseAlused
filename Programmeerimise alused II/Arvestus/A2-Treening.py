def failist_sõnastikku(failinimi):
    with open(failinimi, encoding='UTF-8') as f:
        andmed = {}
        for rida in f:            
            rida = rida.strip().split(' ')
            if rida[0] not in andmed:
                andmed[rida[0]] = int(rida[1])
            else:
                andmed[rida[0]] += int(rida[1])
    return andmed

def kalorid_väiksemad(põletatudKCal, sõnastik):
    tegevused = []
    for rida in sõnastik:        
        if sõnastik[rida] < põletatudKCal:
            tegevused += [rida]
    return tegevused

failinimi = input('Sisestage failinimi: ') # töötab kaasasoleva failiga 'treeningud.txt'
kaloripiir = int(input('Sisestage arv, millest väiksema kalorite arvuga tegevusi näidata: '))

for tegevus in kalorid_väiksemad(kaloripiir,
                                 failist_sõnastikku(failinimi)):
    print(tegevus)