def kasOnTransponeeritudMaatriks(transponeeritavMaatriks, võrreldavMaatriks):
    uusMaatriks = []
    for rida in transponeeritavMaatriks:        
        for i in range(len(rida)):
            uusRida = [rida[i]]

            if len(uusMaatriks) == i:
                uusMaatriks += [uusRida]
            else:
                uusMaatriks[i] += uusRida
    
    if uusMaatriks == võrreldavMaatriks:
        return True
    else:
        return False            
               
maatriks_1 = [[1, 2], [3, 4], [5, 6]]
maatriks_2 = [[1, 3, 5], [2, 4, 6]]
# True

maatriks_3 = [[1, 2], [1, 1]]
maatriks_4 = [[1, 2, 3], [2, 2, 3]]
# False

maatriks_5 = [[1, 2, 3, 4], [1, 1, 7, 7]]
maatriks_6 = [[1, 1],[2, 1], [3, 7], [4, 7]]
# True

print(kasOnTransponeeritudMaatriks(maatriks_5, maatriks_6))