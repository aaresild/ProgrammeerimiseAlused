taishaalikud = ["a","e","i","o","u","õ","ä","ö","ü"]
sonade_tabel = [["piim", "kommikarp", "lillkapsas"],
                ["leib", "hakkliha", "sink"],
                ["makaronid", "mandariinid", "tee"]]
# tulba_pikimad => ['makaronid', 'mandariinid', 'lillkapsas']
# enim_taishaalikuid => mandariinid

def tulba_pikimad(järjend):
    for rida in järjend:
        pikimad = järjend[0]
        for i in range(len(rida)):                        
            if len(pikimad[i]) < len(rida[i]):
                pikimad[i] = rida[i]           
    return pikimad
        
def enim_taishaalikuid(järjend):
    sõneindeks_täshäälikuid = (0, 0)    
    for i in range(len(järjend)):
        täishäälikuid_sõnes = 0
        for täht in järjend[i]:            
            for häälik in taishaalikud:
                if täht == häälik:
                    täishäälikuid_sõnes += 1
        if sõneindeks_täshäälikuid[1] < täishäälikuid_sõnes:
            sõneindeks_täshäälikuid = (i, täishäälikuid_sõnes)
    return järjend[sõneindeks_täshäälikuid[0]]
     

print(enim_taishaalikuid(tulba_pikimad(sonade_tabel)))