def summa(struktuur):
    tulemus = 0
    for element in struktuur:
        if isinstance(element, list):
            tulemus += summa(element)
        else:
            tulemus += element
    return tulemus
 
print(summa([1, [2], [3, 4]]))
#print(summa([1, [2, [[3, 4], [5, 6]]], [7, 8, 9]]))
#print(summa([1, 2, 3, 4]))