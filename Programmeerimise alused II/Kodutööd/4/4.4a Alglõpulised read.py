def on_alglopuline(reake):
    if len(reake) > 1:
        if reake[0] > reake[-1]:
            return True
    return False

jarjend_ruudus = [[4, 3, 2], [-1, 0]]

alglopulisi = 0
for rida in jarjend_ruudus:
    alglopulisi += on_alglopuline(rida)
print("Alglõpulisi ridu on " + str(alglopulisi))