def absoluutne_tabel(järj):
    uusJärj = []
    for i in range(len(järj)):
        rida = []
        for j in range(len(järj[i])):
            rida += [abs(järj[i][j])]
        uusJärj += [rida]
    return uusJärj

def absolutiseeri_tabel(järj):
    for i in range(len(järj)):
       for j in range(len(järj[i])):
            järj[i][j] = abs(järj[i][j])
   