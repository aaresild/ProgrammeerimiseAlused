def on_kasvav(reake):
    kasvav = False
    if len(reake) > 0:
        kasvav = True
        eelmine = reake[0]
        for el in reake[1:]:
            if el <= eelmine:
                kasvav = False
                break
            eelmine = el
    if kasvav:
        return 1
    else:
        return 0            
    
jarjend_ruudus = [[4, 3, 2], [-1, 0],[]]

kasvavaid = 0
for rida in jarjend_ruudus:
    kasvavaid += on_kasvav(rida)
    
print("Kasvavaid ridu on " + str(kasvavaid))