def nurkademanguks_vaja(mäng):
    vaja = []
    for i in range(len(mäng)):
        for j in range(len(mäng)):
            if (i == 0 or i == len(mäng) -1) and (j == 0 or j == len(mäng) - 1) and mäng[i][j] != 'X':
                vaja.append(mäng[i][j])
    return vaja

def diagonaalidemanguks_vaja(mäng):
    vaja = []
    for i in range(len(mäng)):
        for j in range(len(mäng)):
            if i == j and mäng[i][j] != 'X':
                vaja.append(mäng[i][j])
        if mäng[i][len(mäng) - i - 1] != 'X':
            if mäng[i][len(mäng) - i - 1] not in vaja:
                vaja.append(mäng[i][len(mäng) - i - 1])
    return vaja

def taismanguks_vaja(mäng):
    vaja = []
    for i in range(len(mäng)):
        for j in range(len(mäng)):
            if mäng[i][j] != 'X':
                vaja.append(mäng[i][j])
    return vaja