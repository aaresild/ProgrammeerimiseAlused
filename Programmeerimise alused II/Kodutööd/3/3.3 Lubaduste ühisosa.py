def kooslubajad(konnad):
    ühisosa = (0, 1)
    vahekordaja = 0
    for i in range(len(konnad)):
        for j in range(len(konnad)):
            if i != j and len(konnad[i] & konnad[j]) > vahekordaja:
                vahekordaja = len(konnad[i] & konnad[j])
                ühisosa = (i, j)
    return ühisosa