failinimi = input("Sisesta failinimi: ")
sisestaÜletajad = input("Sisesta piiriületajad: ").upper().split(' ')

def failist_sonastik(failinimi):
    f = open(failinimi, encoding="UTF-8")
    failisisu = []
    for i in f:
        i.strip('\n')
        failisisu += i.split(' ')
    f.close()
        
    sõnastik = {}
    for tähis in range(0, len(failisisu), 2):
        sõnastik[failisisu[tähis]] = failisisu[tähis + 1].strip()        
    return sõnastik

def tahised_nimedeks(riikideTähised, andmedFailist = failist_sonastik(failinimi)):
    riiginimed = []
    for tähis in riikideTähised:
        if tähis in andmedFailist:
            riiginimed += [andmedFailist[tähis]]
        else:
            riiginimed += [None]
    return riiginimed

for päritolu in tahised_nimedeks(sisestaÜletajad):
    if päritolu != None:
        print(päritolu)
    else:
        print('Tundmatu maa')

  