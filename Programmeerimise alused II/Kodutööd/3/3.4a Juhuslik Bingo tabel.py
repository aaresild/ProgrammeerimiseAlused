from random import sample

def juhuslik_bingo():
    toormaterjal = [sample(range(1, 16), 5), sample(range(16, 31), 5), sample(range(31, 46), 5), sample(range(46, 61), 5), sample(range(61, 76), 5)]
    bingo = []    
    for i in range(5):
        rida = []
        for j in range(5):
            rida += [toormaterjal[j][i]]
        bingo += [rida]
    
    return bingo