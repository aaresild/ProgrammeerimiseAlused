print('Tere\nSelle programmiga saad teada, kui palju oli mingil kindlal aastal, sind huvitavas vanuses esmasünnitajaid.\n\nStatistika on ajavahemikust 1989 - 2016 ja emade kohta vanuses 13 - 49.\n')

def sünniaasta():
    aasta = int(input('Sisesta sulle huvipakkuv aasta: '))
    
    if 1989 <= aasta <= 2016:
        return aasta
    else:
        print('Sünniaasta peab olema vahemikus 1989 - 2016.')
        return sünniaasta()
sünniaasta = sünniaasta()

def vanus():
    emaVanus = int(input('Sisesta esmasünnitanud emade vanus: '))
   
    if 13 <= emaVanus <= 49:
        return emaVanus
    else:
        print('Emade vanus peab olema vahemikus 13 - 49.')
        return vanus()
vanus = vanus()

f = open('RV129s.csv', encoding="UTF-8")

sünniandmed = []
for rida in f:   
    sünniandmed.append(rida.strip().split(';'))    
f.close()

aastaindeks = 0
for i in range(len(sünniandmed)):
    if str(sünniaasta) == sünniandmed[i][0]:
        aastaindeks = i
        break

vanuseIndeks = 0
for i in range(len(sünniandmed[0])):
    if str(vanus) == sünniandmed[0][i]:
        vanuseIndeks = i
        break

if sünniandmed[aastaindeks][vanuseIndeks] == '0':
    print('Aastal ' + str(sünniaasta) + ' ei olnud ühtegi ' + str(vanus) + '-aastast esmasünnitajat.')
else:
    print('Aastal ' + str(sünniaasta) + ' oli ' + str(vanus) + '-aastaseid esmasünnitajaid '+ str(sünniandmed[aastaindeks][vanuseIndeks]) + '.')
