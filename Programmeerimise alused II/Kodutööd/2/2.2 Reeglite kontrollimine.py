def on_bingo_tabel(maatriks):
    if len(maatriks) != 5:
        return False
    kontrollindeks = 0
    for rida in maatriks:
        
        if len(rida) != 5:
            return False
        if rida[0] in range(1, 16) and rida[1] in range(16, 31) and rida[2] in range(31, 46) and rida[3] in range(46, 61) and rida[4] in range(61, 76):
            kontrollindeks += 1
    if kontrollindeks == 5:
        return True
    else:
        return False

    