def leidub_anagramm(sõned):
    for el in sõned:
        for i in range(len(el)):
            if len(el) > 2 and i == 0 and sorted(el[i]) == sorted(el[i + 1]):
                return True
            elif i != 0 and (i == len(el) -1 and sorted(el[i]) == sorted(el[i - 1])):
                return True    
            elif i != len(el) -1:
                if (sorted(el[i]) == sorted(el[i - 1] + el[i + 1])):
                    return True
            elif i != 0 and sorted(el[i]) == sorted(el[i - 1]):
                return True+1
            elif len(el) == 1 and el[i] == '':
                return True
    return False