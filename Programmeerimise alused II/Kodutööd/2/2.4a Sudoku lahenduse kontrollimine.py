f = input('Sisesta failinimi: ')

fail = open(f)
maatriks = []
for i in fail:
    failistListi = []
    read = i.split()
    for rida in read:
        failistListi += [int(rida)]
    maatriks += [failistListi]
fail.close()
   
def hor_read(maatriks):
    for i in range(9):
        if sorted(maatriks[i]) != list(range(1, 10)):
            return False
        else:
            return True

def vert_read(maatriks):
    for i in range(9):
        vert_rida = []
        for j in range(9):
            vert_rida += [maatriks[j][i]]
        if sorted(vert_rida) != list(range(1, 10)):
            return False
        else:
            return True

def kastid_korras(maatriks):
    # Vaatame igat 3x3 kasti
    for rea_nurk in range(0, 9, 3):
        for veeru_nurk in range(0, 9, 3):
            # Iga kasti korral kogume tema elemendid järjendisse 'kast'
            kast = []
            for i in range(3):
                for j in range(3):
                    kast.append(int(maatriks[rea_nurk + i][veeru_nurk + j]))
            # Ja kontrollime, kas elemendid on korrektsed
            if sorted(kast) != list(range(1, 10)):
                return False
    return True
if hor_read(maatriks) and vert_read(maatriks) and kastid_korras(maatriks):
    print('OK')
else:
    print('VIGA')