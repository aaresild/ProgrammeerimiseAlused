def voitis_nurkademangu(maatriks):
    if maatriks[0][0] == 'X' and maatriks[0][4] == 'X' and maatriks[4][0] == 'X' and maatriks[4][4] == 'X':
        return True
    else:
        return False
    
def voitis_diagonaalidemangu(maatriks):
    if x_peadiagonaalil(maatriks) + x_korvaldiagonaalil(maatriks) == 10:
        return True
    else:
        return False
    
def x_peadiagonaalil(maatriks):
    peadiagonaaliKordaja = 0
    for i in range(len(maatriks)):
        for j in range(len(maatriks)):
            if maatriks[i][j] == 'X' and i == j:
                peadiagonaaliKordaja += 1
    return peadiagonaaliKordaja
  

def x_korvaldiagonaalil(maatriks):
    korvaldiagonaaliKordaja = 0
    for i in range(len(maatriks)):
        for j in range(len(maatriks)):
            if maatriks[j][i] == 'X' and i + j == 4:
                korvaldiagonaaliKordaja += 1
    return korvaldiagonaaliKordaja
 
def voitis_taismangu(maatriks):
    kordaja = 0
    for i in range(len(maatriks)):
        for j in range(len(maatriks)):
            if maatriks[j][i] == 'X':
                kordaja += 1
    if kordaja == 25:
        return True
    else:
        return False
    