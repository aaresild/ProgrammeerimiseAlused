import pandas as pd

def taienda_tabelit(uuedAndmed):
    uuedAndmed = uuedAndmed.drop(['2012'], axis=1)
    uuedAndmed['Keskmine'] = uuedAndmed.mean(1, numeric_only=True)
    return uuedAndmed

def raamatukogud(nimi):
    
    andmed = pd.read_csv(nimi, delimiter=';')
    failinimi = nimi.strip('.csv')
    taienda_tabelit(andmed).to_csv(failinimi +'_uus.csv', sep=';', encoding='utf-8')
