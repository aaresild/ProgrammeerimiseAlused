def paarissumma(arv):
    if arv % 2 == 1:
        arv -= 1
    if arv <= 0:
        return 0
    else:
        return arv + paarissumma(arv - 2)

            