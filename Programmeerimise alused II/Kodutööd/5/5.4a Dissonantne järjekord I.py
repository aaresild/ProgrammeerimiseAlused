def dissonantside_arv(arvamusteJärj, lubatudErinevus):
    dissonantsideArv = 0
    for i in range(len(arvamusteJärj)):
        if i != len(arvamusteJärj) - 1:
            if arvamusteJärj[i] * arvamusteJärj[i + 1] < 0 and \
                abs(arvamusteJärj[i] - arvamusteJärj[i + 1]) > lubatudErinevus:
                dissonantsideArv += 1
    return dissonantsideArv

