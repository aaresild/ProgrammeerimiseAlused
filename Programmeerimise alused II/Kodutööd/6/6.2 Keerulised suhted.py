def seosta_lapsed_ja_vanemad(lapsed, nimed):
    lastefail = open(lapsed)
    vanem_laps = []    
    for rida in lastefail:
        vanem_laps += [rida.strip().split(' ')]
    lastefail.close()
    
    nimefail = open(nimed, encoding="UTF-8")    
    isikud = {}
    for rida in nimefail:      
        isikud[rida[0:11]] = rida[12:].strip()
    nimefail.close()
    
    for ik, nimi in isikud.items():
        lapsed = set()
        for i in range(len(vanem_laps)):
            for j in range(len(vanem_laps[i])):
                if vanem_laps[i][j] == ik:
                    vanem_laps[i][j] = nimi
    
    vanemad_lastega = {}
    for seos in vanem_laps:
        if seos[0] not in vanemad_lastega:
            laps = set()
            laps.add(seos[1])
            vanemad_lastega[seos[0]] = laps
        elif seos[0] in vanemad_lastega:
            laps.add(seos[1])
            vanemad_lastega[seos[0]] = laps                   
                    
    return vanemad_lastega                   
    
#print(seosta_lapsed_ja_vanemad('lapsed.txt', 'nimed.txt'))