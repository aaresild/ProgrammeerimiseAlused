def kuhu_saab(väljumised, lennud):    
    sinna_saab = set()
    for väljumine in väljumised:        
        if väljumine in lennud.keys():               
            sinna_saab.update(lennud[väljumine])
    return sinna_saab               
    
def leidub_lennuplaan(lähtelinn, sihtlinn, lennud):
    if lähtelinn not in lennud.keys():
        return False
    
    kõik_sihtkohad = set()
    [kõik_sihtkohad.update(linn) for linn in [sihtlinnad_hulkadena for sihtlinnad_hulkadena in lennud.values()]]
    juba_külastatud = {lähtelinn}
    
    while len(kõik_sihtkohad) > len(juba_külastatud):
        kuhu_saab_lennata = kuhu_saab(juba_külastatud, lennud)
        
        if sihtlinn in kuhu_saab_lennata:
            return True
        else:
            if len(juba_külastatud) < len(juba_külastatud | kuhu_saab_lennata):
                juba_külastatud.update(kuhu_saab_lennata)
            else:
                return False
            if len(kõik_sihtkohad) == len(juba_külastatud):
                return False        
    
##andmed = {'Berliin': {'London', 'Tapa'},
##          'Tallinn': {'Berliin', 'London'},          
##          'London': {'Berliin', 'Padiski'},
##          'Padiski': {'London', 'Moora'},
##          'Moora': {'Jüri', 'Helsingi'},
##          'Helsingi': {'Tartu', 'Tallinn'}}
##andmed1 = {'London': {'Pariis', 'Berliin'},
##           'Tallinn': {'Berliin'},
##           'Pariis': {'London', 'Nice', 'Berliin'},
##           'Berliin': {'Nice'}}
##andmed2 = {'London': {'Pariis', 'Berliin'},
##           'Tallinn': {'Berliin'},
##           'Pariis': {'London', 'Nice', 'Berliin'},
##           'Berliin': {'Nice'}}
##
##print(leidub_lennuplaan('Berliin', 'Tallinn', andmed)) # True, saab ümberistumistega lennata
##print(leidub_lennuplaan('Berliin', 'Tallinn', andmed1)) # False, Nice'st lende edasi ei lähe
##print(leidub_lennuplaan('Tartu', 'Berliin', andmed2)) # False, Tartut pole lähtelinnade hulgas