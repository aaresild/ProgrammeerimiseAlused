def pikim_pikkus(järjend):
    def järjendid(järjend):
        pikkused = []
        if isinstance(järjend, list):
            pikkused += [len(järjend)]
        for järts in järjend:
            if isinstance(järts, list):
                pikkused += järjendid(järts)
        return pikkused
    return max(järjendid(järjend))
