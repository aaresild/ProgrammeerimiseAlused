def kas_saab(lähe, siht, andmebaas, külastatud):
    if lähe == siht:
        return [siht]
    if lähe not in külastatud:
        külastatud += [lähe]
    else:
        return
    kõik_sihtkohad = set()
    [kõik_sihtkohad.update(linn) for linn in [sihtlinnad_hulkadena for sihtlinnad_hulkadena in andmebaas.values()]]
    if len(külastatud) == len(kõik_sihtkohad):
        return    
    else:
        for linn in andmebaas[lähe]:
            if (linn in andmebaas.keys() or linn == siht) and linn not in külastatud[0:-1]:
                teekond = kas_saab(linn, siht, andmebaas, külastatud)
                if isinstance(teekond, list):
                    return [lähe] + teekond

def koosta_lennuplaan(lähe, siht, andmebaas):    
    if lähe not in andmebaas.keys():
        return
    
    külastatud = [lähe]        
            
    for linn in andmebaas[lähe]:
        if linn in andmebaas.keys():
            teekond = kas_saab(linn, siht, andmebaas, külastatud)
            if isinstance(teekond, list):
                teekond = [lähe] + teekond            
                if teekond[-1] == siht:
                    return teekond
    return

##andmed = {'Berliin': {'London', 'Tapa'},
##          'Tallinn': {'Berliin', 'London'},          
##          'London': {'Berliin', 'Paldiski'},
##          'Paldiski': {'London', 'Moora', 'Berliin', 'Tallinn', 'Helsingi'},
##          'Moora': {'Jüri', 'Helsingi'},
##          'Helsingi': {'Tartu', 'Tallinn'},
##          'Jüri': {'Kunda'},
##          'Kunda': {'Haapsalu'}}
##print(koosta_lennuplaan('Tallinn', 'Berliin', andmed))