def kala_kaal(kala_pikkus, fti):
    kaal = kala_pikkus**3 * fti /100
    return round(kaal)

fail = input("Sisesta failinimi: ")
f = open(fail)
alammõõt = int(input("Sisesta kala alammõõt: "))
fti = float(input("Sisesta Fultoni indeks: "))
järjend = []

for i in f:
    järjend.append(int(i.strip('\n')))
for i in järjend:
    if i < alammõõt:
        print('Kala lasti vette tagasi')
    else:
        print('Püüti kala kaaluga',kala_kaal(i, fti), 'grammi')
purikas = round((kala_kaal(max(järjend), fti) / 1000), 3)

print('Raskeim kala kaalub', purikas, 'kg')